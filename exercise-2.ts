import { Superhero } from "./exercise-1";

// Clase Superhero
class SuperheroClass implements Superhero {
  // Constructor y propiedades a definir según la interfaz
  constructor() {

  }

  // Método introduce a implementar:
  introduce(): string {
    return `Hi, I am ${this.alias}, also known as ${this.name}.`;
  }
}

// Testeo
const batman = new SuperheroClass(
  "Bruce Wayne",
  "Batman",
  ["intelligence", "combat skills", "wealth"],
  35
);

console.log(
  batman.introduce() === "Hi, I am Batman, also known as Bruce Wayne."
); // true
